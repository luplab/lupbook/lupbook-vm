# SPDX-License-Identifier: GPL-2.0+
# Copyright (c) 2021 Joël Porquet-Lupine and Garrett Hagopian

## Build targets
all: lupbookvm tests


## Configuration
GCC ?= gcc
EMCC ?= emcc
CROSS_COMPILE ?= riscv64-unknown-linux-gnu-
NJOBS ?= 8
BUILD_DIR ?= build

CONF_DIR := conf
SRC_DIR := ext
ARTF_NAMESPACE := LupBookVM


## Helper functions

# Function to update a timestamp
# $1: path to dependency
# $2: path to timestamp file
define check_commit
	if [ "$(shell git -C $(1) rev-parse HEAD)" != "$$(cat $(2) 2>/dev/null)" ]; then \
		echo "$$(git -C $(1) rev-parse HEAD)" > $(2); \
	fi
endef

# Function to generate JavaScript for the `binaries` array
# $1: binary file to compress
# $2: output JS file
define gen_binary_js
	echo "var $(ARTF_NAMESPACE) = (function (my) {" \
	"if (typeof my.binaries === 'undefined') my.binaries = {};" \
	"my.binaries['$(notdir $(1))'] =" \
	"'data:application/octet-stream;base64,$$(lz4 -cl $(1) | base64 -w 0)';" \
	"return my;" \
	"}($(ARTF_NAMESPACE) || {}));" \
	> $(2)
endef


## Submodule definitions

SRC_TEMU ?= $(SRC_DIR)/temu
SRC_BBL ?= $(SRC_DIR)/riscv-pk
SRC_LINUX ?= $(SRC_DIR)/linux
SRC_BR2 ?= $(SRC_DIR)/buildroot

BUILD_TEMU := $(BUILD_DIR)/temu
BUILD_BBL := $(BUILD_DIR)/riscv-pk
BUILD_LINUX := $(BUILD_DIR)/linux
BUILD_BR2 := $(BUILD_DIR)/buildroot

ARTF_TEMU := temu_rv64.js
ARTF_BBL := bbl_rv64.js
ARTF_LINUX := linux_rv64.js
ARTF_BR2 := rootfs_rv64.js


## Path resolution
override BUILD_DIR := $(abspath $(BUILD_DIR))
override SRC_DIR := $(abspath $(SRC_DIR))
override CONF_DIR := $(abspath $(CONF_DIR))

override SRC_TEMU := $(abspath $(SRC_TEMU))
override SRC_BBL := $(abspath $(SRC_BBL))
override SRC_LINUX := $(abspath $(SRC_LINUX))
override SRC_BR2 := $(abspath $(SRC_BR2))

override BUILD_TEMU := $(abspath $(BUILD_TEMU))
override BUILD_BBL := $(abspath $(BUILD_BBL))
override BUILD_LINUX := $(abspath $(BUILD_LINUX))
override BUILD_BR2 := $(abspath $(BUILD_BR2))


## Lupbookvm definitions

# JS front-end API
ARTF_API := vm_api.js

# All the JS modules
LBVM_MODULE_DEPS := $(addprefix $(BUILD_DIR)/, \
    $(ARTF_TEMU) $(ARTF_API) $(ARTF_BBL) $(ARTF_LINUX) $(ARTF_BR2))
LBVM_MODULE := $(BUILD_DIR)/lupbookvm.js


## Submodules rules

# Rebuild detection
$(BUILD_DIR)/%/.commit: $(SRC_DIR)/% FORCE
	@mkdir -p $(dir $@)
	$(if $(filter-out $(shell cat $@ 2>/dev/null),$(shell git -C $< rev-parse HEAD)),\
		git -C $< rev-parse HEAD > $@,\
		@echo "Skipping $(basename $<)")

# Temu build
$(BUILD_TEMU)/$(ARTF_TEMU): $(BUILD_TEMU)/.commit
	$(MAKE) -C $(SRC_TEMU) O=$(BUILD_TEMU) CONF_FILE=$(CONF_DIR)/temu.mk js
	@touch $@

# BBL build
$(BUILD_BBL)/bbl.bin: $(BUILD_BBL)/.commit
	cd $(BUILD_BBL); \
		$(SRC_BBL)/configure --host=$(patsubst %-,%,$(CROSS_COMPILE)) && \
		$(MAKE)
	@touch $@

# Linux build
$(BUILD_LINUX)/arch/riscv/boot/Image: $(BUILD_LINUX)/.commit
	cp $(CONF_DIR)/linux_rv64_v510_config $(BUILD_LINUX)/.config
	$(MAKE) -C $(SRC_LINUX) O=$(BUILD_LINUX) ARCH=riscv CROSS_COMPILE=$(CROSS_COMPILE) olddefconfig
	$(MAKE) -C $(SRC_LINUX) O=$(BUILD_LINUX) ARCH=riscv CROSS_COMPILE=$(CROSS_COMPILE) -j $(NJOBS)
	@touch $@

# Buildroot
$(BUILD_BR2)/images/rootfs.cpio: $(BUILD_BR2)/.commit
	$(MAKE) -C $(SRC_BR2) O=$(BUILD_BR2) BR2_EXTERNAL=../buildroot-gcc lupbook_gcc_defconfig
	$(MAKE) -C $(SRC_BR2) O=$(BUILD_BR2) -j $(NJOBS)
	@touch $@

# Temu JS artifact
$(BUILD_DIR)/$(ARTF_TEMU): $(BUILD_TEMU)/$(ARTF_TEMU)
	cp $< $@

# BBL JS artifact
$(BUILD_DIR)/$(ARTF_BBL): $(BUILD_BBL)/bbl.bin
	$(call gen_binary_js,$<,$@)

# Linux JS artifact
$(BUILD_DIR)/$(ARTF_LINUX): $(BUILD_LINUX)/arch/riscv/boot/Image
	$(call gen_binary_js,$<,$@)

# Buildroot JS artifact
$(BUILD_DIR)/$(ARTF_BR2): $(BUILD_BR2)/images/rootfs.cpio
	$(call gen_binary_js,$<,$@)

## JS API

# Front-end API
$(BUILD_DIR)/$(ARTF_API): api/*.js
	mkdir -p $(dir $@)
	cat $^ | sed "s/\$$JS_NAMESPACE/$(ARTF_NAMESPACE)/g" > $@

# Entire JS module
lupbookvm: $(LBVM_MODULE)
$(LBVM_MODULE): $(LBVM_MODULE_DEPS)
	cat $^ > $@

# Tests
HTML_TESTS := $(addprefix $(BUILD_DIR)/, \
    test_core.html test_exec.html test_session.html)
tests: lupbookvm $(HTML_TESTS)
$(BUILD_DIR)/test_%.html: test/%.html test/_template.html $(LBVM_MODULE)
	sed -e "/{{LBVM_MODULE}}/{r $(LBVM_MODULE)" -e "d}" \
	    -e "/{{LBVM_TEST_BODY}}/{r $<" -e "d}" \
		test/_template.html > $@

# Clean up
clean: FORCE
	rm -rf $(BUILD_DIR)

# Phony rules
FORCE:
.PHONY: FORCE
