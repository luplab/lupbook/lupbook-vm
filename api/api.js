/*
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * Copyright (c) 2021-2024 LupLab
 *
 * This file defines the JS API for our entire virtual machine, which is
 * composed of three stacked layer: Core, Exec, and Session.
 */

/*
 * The Core layer has three sub-APIs:
 * - `Emu` is the API facing the WASM emulator.
 * - `Client` is the API facing the webpage.
 * - `Core` is the API with the `Exec` layer.
 */
var $JS_NAMESPACE = (function (my) {
  /*
   * Private members
   */
  let lupbookEmu = null;

  /* By default, log everything coming from the emulator in the console */
  let debugConsoleLine = '';
  debugConsoleWrite = function(str) {
    const parts = str.split('\n');

    /* Append first part to existing line */
    debugConsoleLine += parts[0];

    for (let i = 1; i < parts.length; i++) {
      /* Print line ending with '\n' */
      console.log(debugConsoleLine);
      /* Accumulate next part */
      debugConsoleLine = parts[i];
    }
  };

  /* Default console dimensions (typical VT100 dimensions) */
  debugConsoleGeometry = () => {
    return { width : 80, height : 24, };
  };

  /*
   * Core API (typically for higher API layers)
   */
  const Core = {};
  my.Core = Core;

  /* By default, provide empty handlers if emulator doesn't provide them */
  Core.consoleQueue = (c) => void 0;
  Core.consoleResize = () => void 0;
  Core.fsImportFile = (fpath, fname, buf, len) => void 0;

  /*
   * API for the JS client
   */
  const Client = {};
  my.Client = Client;

  /* Returns Promise that resolves when VM starts booting. */
  Client.boot = async function({
    /* Function receiving everything what's printed to the console for debug */
    consoleWriteDebug = debugConsoleWrite,
    /* Size of the debug console */
    consoleGeometryDebug = debugConsoleGeometry } = {})
  {
    debugConsoleWrite = consoleWriteDebug;
    debugConsoleGeometry = consoleGeometryDebug;

    /* Initialize WASM runtime */
    lupbookEmu = await createLupBookEmu({
      noInitialRun: true,
      onAbort: err => {
        throw new Error('Emulator aborted: ' + err);
      },

      /* Custom object internally expected by our emulator */
      VM: my.Emu,
    });

    const emuArgs = [
      /* Emulator arguments */
      '-machine', 'riscv64',
      '-bios', 'bbl.bin',
      '-kernel', 'Image',
      '-drive', 'rootfs.cpio',
      '-append', [
        /* Kernel command line arguments */
        'earlycon=sbi',
        'console=hvc0',
        'root=root',
        'rootfstype=9p',
        'rootflags=trans=virtio',
        'ro',
        'TZ=' + Intl.DateTimeFormat().resolvedOptions().timeZone
      ].join(' ')
    ];

    /* Call main */
    if (lupbookEmu.callMain(emuArgs) !== 0)
      throw new Error('callMain() failed');

    return lupbookEmu;
  }

  /*
   * API for the emulator
   */
  const Emu = {};
  my.Emu = Emu;

  /* Emulator initialization. Provides emulator functions our API can call. */
  Emu.register = function({
    /* Send character to emulator's console */
    consoleQueue = Core.consoleQueue,
    /* Resize emulator's console */
    consoleResize = Core.consoleResize,
    /* Import file into emulator's filesystem */
    fsImportFile = Core.fsImportFile } = {})
  {
    Core.consoleQueue = consoleQueue;
    Core.consoleResize = consoleResize;
    Core.fsImportFile = fsImportFile;
  };

  /* Provide output from the emulator's console */
  Emu.consoleWrite = function(str) {
    debugConsoleWrite(str);

    if (typeof my.Exec !== 'undefined')
      /* XXX We check that my.Exec is defined so that the core API can
       * potentially be used by itself */
      my.Exec.consoleWrite(str);
  };

  /* Get size of terminal output */
  Emu.consoleGeometry = function() {
    return debugConsoleGeometry();
  };

  /* Load file into emulator */
  Emu.loadFile = function(filename) {
    try {
      return lupbookEmu.tryParseAsDataURI(my.binaries[filename]);
    } catch (e) {
      console.error(e);
      return null;
    }
  };

  /* Export file from emulator's filesystem */
  Emu.fsExportFile = function(fpathname, data = null) {
    if (typeof my.Session !== 'undefined')
      my.Session.fsExportFile(fpathname, data);
  };

  return my;
}($JS_NAMESPACE || {}));

/*
 * The Exec layer manages commands being executed in the VM
 */
var $JS_NAMESPACE = (function (my) {
  const Exec = {};
  my.Exec = Exec;

  /*
   * Private members
   */
  let initialized = false;
  let currentCmdInfo = null;
  const queueCmdInfo = [];
  let outputBuffer = '';

  /* Match full prompt pattern: newline + exit code + # + end of string */
  const promptPattern = /\r\n\d+#\s$/;

  wrapCmd = function(cmdInfo) {
    const cmd = [];

    if (cmdInfo.timeout > 0)
      cmd.push(`timeout ${cmdInfo.timeout} `);

    cmd.push(cmdInfo.rawCmd);

    if (cmdInfo.subShell) {
      cmd.unshift("sh -c '");
      cmd.push("'");
    }

    if (cmdInfo.outPath)
      cmd.push(` >& ${cmdInfo.outPath}`);

    return cmd.join('');
  }

  runCmdShell = function(cmd) {
    /* Type in command and validate with Enter */
    [...cmd, '\n'].forEach(c => my.Core.consoleQueue(c.charCodeAt(0)));
  }

  tryRunNext = function() {
    /* Return if nothing to run or a command is already running */
    if (queueCmdInfo.length === 0 || currentCmdInfo !== null)
      return;
    /* Run next command in queue */
    currentCmdInfo = queueCmdInfo.shift();
    runCmdShell(currentCmdInfo.wrapCmd);
  }

  /*
   * Public API facing Core layer
   */
  Exec.consoleWrite = function(str) {
    /* Output generated outside of us running specific commands is ignored */
    if (!initialized || !currentCmdInfo)
      return;

    /* Add output to current buffer */
    outputBuffer += str;

    /* Seeing the prompt means that the current command has completed */
    if (promptPattern.test(outputBuffer)) {
      /* Lines from the console end with `\r\n` */
      const lines = outputBuffer.split('\r\n');

      /* Parse exit code from the last line (which is the prompt) */
      const exitCodeMatch = lines.pop().match(/^(\d+)#\s$/);
      const exitCode = exitCodeMatch ? parseInt(exitCodeMatch[1]) : null;

      /* Resolve current command */
      const {resolve} = currentCmdInfo;
      resolve({
        exitCode: exitCode,
      /* Reconstruct lines only with `\n` */
        outputShell: lines.join('\n')
      });

      /* Run next command if any */
      currentCmdInfo = null;
      outputBuffer = '';
      tryRunNext();
    }
  };

  /*
   * Public API facing Session layer
   */
  Exec.init = async function() {
    const savedConsoleWrite = Exec.consoleWrite;

    /* Wait prompt appears after the boot */
    await new Promise((resolve, reject) => {
      let buffer = '';

      /* Temporarily hijack consoleWrite function to detect prompt */
      Exec.consoleWrite = (str) => {
        buffer += str;
        if (promptPattern.test(buffer))
          resolve();
      };

    });

    Exec.consoleWrite = savedConsoleWrite;

    initialized = true;
  };

  Exec.run = async function(cmdStr, options = {})
  {
    if (!initialized)
      throw new Error('Exec layer not initialized');

    const cmdDefaults = {
      timeout: 0,           /* Wrap command with timeout */
      outPath: '/dev/null', /* Redirect output (both stdout/stderr) */
      subShell: true,       /* Wrap command in subshell */
    };

    /* Create command object */
    const cmdInfo = {
      rawCmd: cmdStr,
      ...cmdDefaults,
      ...options,
    };

    if (typeof cmdInfo.rawCmd !== 'string')
      throw new Error('Argument cmdStr has to be a string');

    /* Generate what exactly need to be run in the VM */
    cmdInfo.wrapCmd = wrapCmd(cmdInfo);

    /* Run command */
    const result = await new Promise(resolve => {
      /* Register resolve callback in cmd object */
      cmdInfo.resolve = resolve;

      /* Push command in queue and try to execute it right away */
      queueCmdInfo.push(cmdInfo);
      tryRunNext();
    });

    return result;
  };

  return my;
}($JS_NAMESPACE || {}));

/*
 * Session layer
 */
var $JS_NAMESPACE = (function (my) {
  const Session = {};
  my.Session = Session;

  /*
   * Private members
   */
  /* Note that this path *has to be* on the CPIO FS for the file import/export
   * to work! */
  const BASE_PATH  = '/var/sessions';

  const activeSessions = new Map();
  const pendingFileExports = new Map();

  /* Ensure data is represented as a char array before sending it to the VM */
  convertToUint8Array = function(data) {
    if (data instanceof Uint8Array)
      return data;
    if (typeof data === 'string')
      return new TextEncoder().encode(data);
    throw new Error('Invalid data format');
  };

  uploadFileVM = function(fpath, fname, buf) {
    /* Send file to emulator's FS */
    const ret = my.Core.fsImportFile(fpath, fname, buf, buf.length);
    if (ret < 0)
      /* Note that we can only import file onto the CPIO FS */
      throw new Error(`Cannot import file ${fpath}/${fname}: error ${ret}`);
  };

  downloadFileVM = async function(fpathname) {
    if (pendingFileExports.has(fpathname))
      throw new Error(`File ${fpathname} is already being exported`);

    const fileDataProcess = new Promise(resolve => {
      /* Associate the promise's resolve to the file */
      pendingFileExports.set(fpathname, resolve);
    }).then(data => new TextDecoder().decode(data));

    /* Send request to emulator to export the file */
    await my.Exec.run(String.raw`echo export_file "${fpathname}" > /.fscmd`,
      { subShell: false, outPath: null });

    /* Wait for the export to resolve */
    const fileData = await fileDataProcess;

    return fileData;
  }

  /*
   * Public API facing the lower layers
   */
  Session.fsExportFile = function(fpathname, data) {
    /* Resolve promise set for this file if any */
    const exportResolve = pendingFileExports.get(fpathname);
    if (!exportResolve) {
      console.warn(`Ignoring export of file '${fpathname}'`);
      return;
    }

    exportResolve(data);
    delete pendingFileExports.delete(fpathname);
  };


  /*
   * Public API facing the client
   */
  Session.init = async function() {
    await my.Exec.init();
  }

  Session.open = async function(sid) {
    if (activeSessions.has(sid))
      throw new Error(`Session ${sid} already exists`);

    const session = {
      id: sid,
      basePath: `${BASE_PATH}/${sid}`,
      workPath: `${BASE_PATH}/${sid}/work`,
      redirPath: `${BASE_PATH}/${sid}/redir`,
    };

    const res = await my.Exec.run(
      String.raw`mkdir -p ${session.workPath} ${session.redirPath}`,
      { subShell: false });
    if (res.exitCode !== 0)
      throw new Error(`mkdir failed with exit code ${res.exitCode}: ${res.outputShell}`);

    activeSessions.set(sid, session);
    return sid;
  }

  Session.upload = function(sid, filename, data) {
    const session = activeSessions.get(sid);
    if (!session)
      throw new Error(`Session ${sid} not found`);

    uploadFileVM(session.workPath, filename, convertToUint8Array(data));
  }

  Session.exec = async function(sid, cmd, timeout = 0) {
    const session = activeSessions.get(sid);
    if (!session)
      throw new Error(`Session ${sid} not found`);

    const outPath = [session.redirPath, 'output'].join('/');
    const cmdCwd = String.raw`cd ${session.workPath}; ${cmd}`;

    /* Run command */
    const {exitCode, outputShell} = await my.Exec.run(cmdCwd, {
      outPath: outPath,
      timeout: timeout,
    });

    /* Retrieve command's output */
    const outputCmd = await downloadFileVM(outPath);

    return {
      exitCode,
      outputCmd,
      outputShell,
    };
  }

  Session.debug = async function(sid, enabled) {
    const session = activeSessions.get(sid);
    if (!session)
      throw new Error(`Session ${sid} not found`);

    const msg = `\n*** Welcome to a debugging console! ***\n` +
      `You are currently in directory '${session.workPath}',` +
      ` which contains the following files:`


    /* XXX: clear has a bug in xtermjs currently as the CSI 2J sequence erases
     * the active output instead of scrolling it into the scrollback buffer. See
     * https://github.com/xtermjs/xterm.js/pull/5224 */
    let cmd = enabled ?
      String.raw`cd ${session.workPath} && clear && echo -e "${msg}" && ls` :
      String.raw`cd /`;

    const {exitCode, outputShell} = await my.Exec.run(cmd,
      { subShell: false, outPath: null });
    if (exitCode !== 0)
      throw new Error(`Debugging session failed:\n${outputShell}`);
  }

  Session.close = async function(sid) {
    const session = activeSessions.get(sid);
    if (!session)
      throw new Error(`Session ${sid} not found`);

    await my.Exec.run(String.raw`rm -rf ${session.basePath}`);
    activeSessions.delete(sid);
  }

  return my;
}($JS_NAMESPACE || {}));

var LupBookEmu = $JS_NAMESPACE.Emu;

